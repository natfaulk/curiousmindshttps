var app = angular.module('myApp', [])
app.controller('auth', ['$scope', '$http', '$location', '$timeout', function($s, $http, $location, $timeout) {
  $s.page='login'
  $s.error = {
    suppw: false,
    supusername: false,
    supname: false,
    supmisc: false,
    logpw: false,
    loguser: false,
    datasaved: false,
    datanot: false
  }
  $s.user = {}

  $s.getData = () => {
    let config = {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    }
    $http.post(`/getdata`, {}, config)
    .then((response) => {
      console.log(response.data)
      $s.user = response.data
      if (!$s.user.loggedin) $s.gotoPage('/login')
      else $s.gotoPage('')
    }, (response) => {
      // failure
      console.log('Post failed')
      console.log(response)
    })
  }
  $s.getData()

  $s.checkPage = (_page) => {
    return _page == $location.path()
  }

  $s.gotoPage = (_page => {
    $location.path(_page)
  })

  $s.signup = () => {
    let failed = false
    $s.error.usernametaken = false
    if (!$s.supname) {
      failed = true
      $s.error.supname = true
    } else $s.error.supname = false

    if (!$s.supusername) {
      failed = true
      $s.error.supusername = true
    } else $s.error.supusername = false

    if (!$s.suppw) {
      failed = true
      $s.error.suppw = true
    } else $s.error.suppw = false
    
    if (!failed) {
      let config = {
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        }
      }
      let data = `fullname=${$s.supname}&username=${$s.supusername}&pw=${$s.suppw}`
      $http.post(`/signup`, data, config)
      .then((response) => {
        console.log(response.data)
        if (response.data == 'username already taken') $s.error.usernametaken = true
        else if(response.data == 'ok') {
          $s.supname = ''
          $s.supusername = ''
          $s.suppw = ''
          $s.error.usernametaken = false
          $s.getData()
        }
        $s.error.supmisc = false
      }, (response) => {
        // failure
        console.log('Post failed')
        console.log(response)
        $s.error.supmisc = true
      })
    }
  }

  $s.login = () => {
    let failed = false
    if (!$s.loguser) {
      failed = true
      $s.error.loguser = true
    } else $s.error.loguser = false

    if (!$s.logpw) {
      failed = true
      $s.error.logpw = true
    } else $s.error.logpw = false

    if (!failed) {
      let config = {
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        }
      }
      let data = `username=${$s.loguser}&pw=${$s.logpw}`
      $http.post(`/login`, data, config)
      .then((response) => {
        console.log(response.data)
        if (response.data !== 'logged in') $s.error.login = true
        else {
          $s.error.login = false
          $s.loguser = ''
          $s.logpw = ''
          $s.getData()
          $s.gotoPage('')
        }
      }, (response) => {
        // failure
        console.log('Post failed')
        console.log(response)
        $s.error.login = true
      })
    }
  }

  $s.logout = () => {
    $http.post(`/logout`)
    .then((response) => {
      console.log(response.data)
      $s.user = {loggedin: false}
      $s.gotoPage('/login')
    }, (response) => {
      // failure
      console.log('Post failed')
      console.log(response)
    })
  }

  $s.savedata = () => {
    let config = {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    }
    let data = `data=${encodeURIComponent($s.user.data)}`
    $http.post(`/savedata`, data, config)
    .then((response) => {
      console.log(response.data)
      $s.error.datasaved = true
      $s.error.datanot = false
      $timeout(() => {
        $s.error.datasaved = false
        $s.error.datanot = false
      }, 10000)
    }, (response) => {
      // failure
      console.log('Post failed')
      console.log(response)
      $s.error.datasaved = false
      $s.error.datanot = true
      $timeout(() => {
        $s.error.datasaved = false
        $s.error.datanot = false
      }, 10000)
    })
  }
}])