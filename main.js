const express = require('express')
const fs = require('fs')
const path = require('path')
const bodyParser = require('body-parser')
const crypto = require('crypto')
const mongoose = require('mongoose')
const cookieParser = require('cookie-parser')

const app = express()
let PORT = 3000;
let dbConnected = false

app.use('/static', express.static('statics'))
app.use(cookieParser())
app.use(bodyParser.urlencoded({
  extended: true
}))

let userSchema = mongoose.Schema({
  fullname: String,
  username: {type: String, unique: true},
  pwsalt: String,
  cookiesalt: String,
  pwhash: String,
  data: String,
  created: {type:Date, default:Date.now}
})

let User = mongoose.model('User', userSchema)

let settings = JSON.parse(fs.readFileSync('settings.json', 'utf8'))
PORT = settings.port

mongoose.connect(settings.db)
let db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'))

db.once('open', function() {
  console.log('db connected')
  dbConnected = true
})

app.get('/', (req, res) => res.sendFile(path.join(__dirname, 'statics', 'html', 'index.html')))


// app.get('/login', (req, res) => {
//   res.sendFile(path.join(__dirname, 'statics', 'html', 'index.html'))
// })

app.post('/signup', (req, res) => {
  if (req.body.fullname && req.body.username && req.body.pw) {
    let salt = genSalt()

    User.find({username: req.body.username}, (err, users) => {
      if (err) {
        res.send('db save failed')
        return
      }
      if (users.length == 0) {
        let newUser = new User({
          fullname: req.body.fullname,
          username: req.body.username,
          pwsalt: salt,
          cookiesalt: genSalt(),
          pwhash: genPwHash(req.body.pw, salt),
          data: ''
        })
    
        newUser.save(function (err, newUser) {
          if (err) {
            console.error(err)
            res.send('db save failed')
          } else {
            res.cookie('session', genLoginCookie(req.body.username, newUser.cookiesalt))
            res.send('ok')
          }
        })
      } else {
        res.send('username already taken')
      }
    })
  } else res.send('missing argument')
})

app.post('/login', (req, res) => {
  if (req.body.username && req.body.pw) {
    User.find({username: req.body.username}, (err, users) => {
      if (err) res.send('find users error')
      else {
        if (users.length == 0) res.send(`user doesn't exist`)
        else {
          if (users[0].pwhash != genPwHash(req.body.pw, users[0].pwsalt)) res.send('invalid password')
          else {
            // users[0].cookiesalt = genSalt()
            // users[0].save()
            res.cookie('session', genLoginCookie(req.body.username, users[0].cookiesalt))
            res.send('logged in')
          }
        }
      }
    })
  } else res.send('missing argument')
})

app.post('/savedata', (req, res) => {
  isLoggedIn(req, (valid, user) => {
    if (!valid) res.send('failed')
    else {
      user.data = req.body.data
      user.save()
      res.send('success')
    }
  })
})

app.post('/logout', (req, res) => {
  res.cookie('session', '')
  res.send('success')
})

app.listen(PORT, () => {
  console.log(`Example app listening on port ${PORT}!`)
})

let genSalt = () => {
  return crypto.randomBytes(4).toString('hex')
}

let genPwHash = (pw, salt) => {
  const hash = crypto.createHash('sha256')
  return hash.update(pw + salt).digest('hex')
}

let genLoginCookie = (username, salt) => {
  const hmac = crypto.createHmac('sha256', salt)
  return `${username}|${hmac.update(username).digest('hex')}`
}

app.post('/getdata', (req, res) => {
  res.setHeader('Content-Type', 'application/json')
  isLoggedIn(req, (valid, user) => {
    if (!valid) res.send(JSON.stringify({loggedin: false}))
    else {
      res.send(JSON.stringify({
        loggedin: true,
        fullname: user.fullname,
        username: user.username,
        data: user.data
      }))
    }
  })
})

let isLoggedIn = (req, _callback) => {
  if (req.cookies.session) {
    checkLoginCookie(req.cookies.session, (_valid, user) => {
      _callback(_valid, user)
    })
  } else _callback(false)
}

let checkLoginCookie = (cookie, _callback) => {
  let t = cookie.split('|')
  if (t.length < 2) {
    _callback(false)
    return
  }
  let username = t[0]
  let hash = t[1]

  User.find({username: username}, (err, users) => {
    if (err) {
      _callback(false)
      return
    }
    if (users.length == 0) {
      _callback(false)
      return
    }
    if (cookie == genLoginCookie(username, users[0].cookiesalt)) _callback(true, users[0])
    else _callback(false)
  })
}